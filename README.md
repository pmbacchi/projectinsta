# README #

This Project is an Instagram Clone demonstrating CocoaPods, SwiftLint, Fabric, and Crashlytics Integration

### What is this repository for? ###

* Instagram Clone for Proof of Concept Use
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* [![codebeat badge](https://codebeat.co/badges/94c6bdd1-71f8-4960-803f-5ccab7c253df)](https://codebeat.co/projects/bitbucket-org-pmbacchi-projectinsta-master)

### How do I get set up? ###

* Clone the Repo
* Do pod install
* Open the Workspace


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Philip M. Bacchi - pbacchi@pobox.com
