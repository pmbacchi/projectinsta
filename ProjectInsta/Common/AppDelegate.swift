//
//  AppDelegate.swift
//  ProjectInsta
//
//  Created by Philip M. Bacchi on 3/8/17.
//  Copyright © 2017 Philip M. Bacchi. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Crashlytics.self])

        return true
    }

}
